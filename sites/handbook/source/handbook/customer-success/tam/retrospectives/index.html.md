---
layout: handbook-page-toc
title: "TAM Retrospectives"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## Retrospectives

The EMEA TAM team are trialing [monthly retrospectives in GitLab issues](https://gitlab.com/gl-retrospectives/tam/-/issues?sort=updated_desc&state=all&label_name[]=retrospective). For more information, see [team retrospectives](https://about.gitlab.com/handbook/engineering/management/group-retrospectives/).

The retrospective issue is created by a scheduled pipeline in the [async-retrospectives](https://gitlab.com/gitlab-org/async-retrospectives) project. For more information on how it works, see the project's README.

A retrospective review is conducted at the end of a milestone with feedback discussed and actions captured in the issue.

**Timeline**

- `M-1 26th`: GitLab Bot opens [Group Retrospective](/handbook/engineering/management/team-retrospectives/) issue for the current milestone.
- `M, 16th`: Team mentioned to encourage contribution.
- `M, Last Friday`: Retrospective review is held.
